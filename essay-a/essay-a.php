<?php
function hitung($string) {
    if(strpos($string, "*")) {
        $string2 = explode("*", $string);
        return $string2[0] * $string2[1];
    } else if (strpos($string, ":")) {
        $string2 = explode(":", $string);
        return $string2[0] / $string2[1];
    } else if (strpos($string, "+")) {
        $string2 = explode("+", $string);
        return $string2[0] + $string2[1];
    } else if (strpos($string, "-")) {
        $string2 = explode("-", $string);
        return $string2[0] - $string2[1];
    } else if (strpos($string, "%")) {
        $string2 = explode("%", $string);
        return $string2[0] % $string2[1];
    }
}

echo hitung("102*2");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");